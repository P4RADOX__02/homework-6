function Humen(name, age) {
    this.name = name;
    this.age = age;
}

Humen.prototype.info = function() {
    return this.name + this.age
}

const humen1 = new Humen("Vlad", 21);
const humen2 = new Humen("Kristina", 24);
const humen3 = new Humen("Vadim", 43);
const humen4 = new Humen("Anna", 15);
const humen5 = new Humen("Anton", 55);
const humen6 = new Humen("Victoria", 32);
const humen7 = new Humen("Ivan", 44);
const humen8 = new Humen("Nazar", 32);

let humen = [humen1, humen2, humen3, humen4, humen5, humen6, humen7, humen8];

console.log(humen.sort((a, b) => a.age - b.age) )


class About {
    constructor (name, age, hobby, city) {
        this.name = name;
        this.age = age;
        this.hobby = hobby;
        this.city = city;
    }

    info() {
        document.write(`Hello my name is ${this.name}, i am ${this.age} years old. My hobby is ${this.hobby}. `)
    }
}

About.prototype.from = function() {
    document.write(`I'm from ${this.city}`)
}



let about = new About("Victor", 32, "diving", "Ukraine");
about.info()
about.from()

document.write("<br/>")

let about1 = new About("Anna", 24, "photography", "Spain");
about1.info()
about1.from()

document.write("<br/>")

let about2 = new About("Ivan", 45, "driving", "Italy");
about2.info()
about2.from()